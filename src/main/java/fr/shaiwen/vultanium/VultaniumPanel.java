package fr.shaiwen.vultanium;

import fr.theshark34.openauth.AuthenticationException;
import fr.theshark34.openlauncherlib.LaunchException;
import fr.theshark34.openlauncherlib.util.Saver;
import fr.theshark34.openlauncherlib.util.ramselector.RamSelector;
import fr.theshark34.swinger.Swinger;
import fr.theshark34.swinger.animation.Animator;
import fr.theshark34.swinger.colored.SColoredBar;
import fr.theshark34.swinger.event.SwingerEvent;
import fr.theshark34.swinger.event.SwingerEventListener;
import fr.theshark34.swinger.textured.STexturedButton;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

public class VultaniumPanel extends JPanel implements SwingerEventListener {
    private Image background = Swinger.getResource("fond.png");

    private Saver saver = new Saver(new File(Vultanium.DIR, "launcher.properties"));

    private JTextField usernameField = new JTextField(saver.get("username"));
    private JPasswordField passwordField = new JPasswordField(saver.get("password"));

    private STexturedButton playButton = new STexturedButton(Swinger.getResource("btn-jouer.png"));
    private STexturedButton quitButton = new STexturedButton(Swinger.getResource("btn-close.png"));
    private STexturedButton reduceButton = new STexturedButton(Swinger.getResource("btn-reduce.png"));

    private STexturedButton settings = new STexturedButton(Swinger.getResource("btn-parametres.png"));

    private STexturedButton twitter = new STexturedButton(Swinger.getResource("twitter.png"));
    private STexturedButton youtube = new STexturedButton(Swinger.getResource("yt.png"));
    private STexturedButton web = new STexturedButton(Swinger.getResource("site.png"));

    private SColoredBar progressBar = new SColoredBar(Swinger.getTransparentWhite(100), Swinger.getTransparentWhite(175));

    RamSelector getRamSelector() {
        return ramSelector;
    }

    private RamSelector ramSelector = new RamSelector(VultaniumFrame.ramFile);

    VultaniumPanel() {
        System.out.println(VultaniumPanel.class.getResourceAsStream(Swinger.getResourcePath() + "/Ge_body.ttf"));
        InputStream is = VultaniumPanel.class.getResourceAsStream(Swinger.getResourcePath() + "/Ge_body.ttf");
        Font customFont = null;
        try {
            customFont = Font.createFont(Font.TRUETYPE_FONT, is);
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }

        this.setLayout(null);

        usernameField.setOpaque(false);
        usernameField.setBorder(null);
        usernameField.setForeground(Color.WHITE);
        assert customFont != null;
        usernameField.setFont(customFont.deriveFont(18f));
        usernameField.setBounds(486, 326, 371, 57);
        usernameField.setCaretColor(Color.WHITE);
        this.add(usernameField);

        passwordField.setOpaque(false);
        passwordField.setBorder(null);
        passwordField.setForeground(Color.WHITE);
        passwordField.setFont(passwordField.getFont().deriveFont(18f));
        passwordField.setBounds(486, 444, 371, 57);
        passwordField.setCaretColor(Color.WHITE);
        this.add(passwordField);

        playButton.setBounds(589, 579, 237, 49);
        playButton.addEventListener(this);
        this.add(playButton);

        quitButton.setBounds(1236, 18, 25, 25);
        quitButton.addEventListener(this);
        this.add(quitButton);

        reduceButton.setBounds(1199, 18, 26, 25);
        reduceButton.addEventListener(this);
        this.add(reduceButton);

        progressBar.setBounds(271, 681, 838, 17);
        this.add(progressBar);

        twitter.setBounds(23, 18, 40, 40);
        twitter.addEventListener(this);
        this.add(twitter);

        youtube.setBounds(23, 78, 40, 40);
        youtube.addEventListener(this);
        this.add(youtube);

        web.setBounds(23, 139, 40, 36);
        web.addEventListener(this);
        this.add(web);

        settings.setBounds(520, 578, 50, 50);
        settings.addEventListener(this);
        this.add(settings);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Swinger.drawFullsizedImage(g, this, background);
    }

    @Override
    public void onEvent(SwingerEvent event) {
        if (event.getSource() == quitButton) {
            Animator.fadeOutFrame(VultaniumFrame.getInstance(), Animator.SLOW, () -> System.exit(0));
        } else if (event.getSource() == reduceButton) {
            VultaniumFrame.getInstance().setState(JFrame.ICONIFIED);
        } else if (event.getSource() == playButton) {
            setFieldEnabled(false);

            if (usernameField.getText().replaceAll(" ", "").length() == 0) {
                JOptionPane.showMessageDialog(this, "Erreur, veuillez entrer un pseudo valide", "Erreur", JOptionPane.ERROR_MESSAGE);
                setFieldEnabled(true);
                return;
            }
            Thread t = new Thread(() -> {
                if (passwordField.getText().equals("")) {
                    try {
                        Vultanium.auth(usernameField.getText(),null);
                    } catch (AuthenticationException e) {
                        Vultanium.getCrashReporter().catchError(e, "Impossible de s'authentifier en crack !");
                        setFieldEnabled(true);
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Vultanium.auth(usernameField.getText(),passwordField.getText());
                    } catch (AuthenticationException e) {
                        Vultanium.getCrashReporter().catchError(e, "Impossible de s'authentifier ou de se connecter avec les serveurs de MOJANG!");
                        setFieldEnabled(true);
                        e.printStackTrace();
                    }
                }

                saver.set("username", usernameField.getText());
                saver.set("password", passwordField.getText());

                try {
                    Vultanium.update();
                } catch (Exception e) {
                    Vultanium.interruptThread();
                    Vultanium.getCrashReporter().catchError(e, "Erreur, impossible de mettre à jour Vultanium !");
                    setFieldEnabled(true);
                    return;
                }

                try {
                    Vultanium.launch();
                } catch (LaunchException e) {
                    Vultanium.getCrashReporter().catchError(e, "Impossible de lancer Vultanium !");
                }
            });

            t.start();
        } else if (event.getSource() == twitter) {
            try {
                Desktop.getDesktop().browse(new URI("https://twitter.com/vultanium"));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        } else if (event.getSource() == youtube) {
            try {
                Desktop.getDesktop().browse(new URI("https://www.youtube.com/channel/UCUFmV9ZL_01H5gqN4luYfmQ"));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        } else if (event.getSource() == settings) {
            ramSelector.display();
        } else if (event.getSource() == web) {
            try {
                Desktop.getDesktop().browse(new URI("https://www.vultanium.fr/"));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private void setFieldEnabled(boolean enabled) {
        usernameField.setEnabled(enabled);
        passwordField.setEnabled(enabled);
        playButton.setEnabled(enabled);
        settings.setEnabled(enabled);
    }

    SColoredBar getProgressBar() {
        return progressBar;
    }
}
