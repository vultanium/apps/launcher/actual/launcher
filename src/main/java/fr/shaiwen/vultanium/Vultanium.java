package fr.shaiwen.vultanium;

import fr.theshark34.openauth.AuthPoints;
import fr.theshark34.openauth.AuthenticationException;
import fr.theshark34.openauth.Authenticator;
import fr.theshark34.openauth.model.AuthAgent;
import fr.theshark34.openauth.model.response.AuthResponse;
import fr.theshark34.openlauncherlib.LaunchException;
import fr.theshark34.openlauncherlib.external.ExternalLaunchProfile;
import fr.theshark34.openlauncherlib.external.ExternalLauncher;
import fr.theshark34.openlauncherlib.minecraft.*;
import fr.theshark34.openlauncherlib.util.CrashReporter;
import fr.theshark34.supdate.BarAPI;
import fr.theshark34.supdate.SUpdate;
import fr.theshark34.supdate.application.integrated.FileDeleter;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;

class Vultanium {
    private static final GameVersion VERSION = new GameVersion("1.12", GameType.V1_8_HIGHER);
    private static final GameInfos INFOS = new GameInfos("Vultanium", VERSION,  new GameTweak[] {GameTweak.FORGE});
    static final File DIR = INFOS.getGameDir();
    private static Thread updateThread;

    private static CrashReporter crashReporter = new CrashReporter("crashes", DIR);

    private static AuthInfos authInfos;

    static void update() throws Exception {
        SUpdate su = new SUpdate("http://launcher-vultanium.fr/launcher/", DIR);
        su.addApplication(new FileDeleter());

        updateThread = new Thread() {
            private int val;
            private int max;

            @Override
            public void run() {

                while (!this.isInterrupted()) {
                    if (BarAPI.getNumberOfTotalBytesToDownload() == 0) {
                        continue;
                    }

                    val = (int) (BarAPI.getNumberOfTotalDownloadedBytes() / 1000);
                    max = (int) (BarAPI.getNumberOfTotalBytesToDownload() / 1000);

                    VultaniumFrame.getInstance().getVultaniumPanel().getProgressBar().setValue(val);
                    VultaniumFrame.getInstance().getVultaniumPanel().getProgressBar().setMaximum(max);
                }
            }
        };

        updateThread.start();
        su.start();
    }

    static void interruptThread() {
        updateThread.interrupt();
    }

    static void launch() throws LaunchException {
        ExternalLaunchProfile profile = MinecraftLauncher.createExternalProfile(INFOS, GameFolder.BASIC,authInfos);
        profile.getVmArgs().addAll(Collections.singletonList("-Dfml.ignoreInvalidMinecraftCertificates=true "));
        profile.getVmArgs().addAll(Collections.singletonList("-Dfml.ignorePatchDiscrepancies=true"));
        profile.getVmArgs().addAll(Arrays.asList(VultaniumFrame.getInstance().getVultaniumPanel().getRamSelector().getRamArguments()));
        ExternalLauncher launcher = new ExternalLauncher(profile);

        Process p = launcher.launch();

        try {
            Thread.sleep(5000L);
            VultaniumFrame.getInstance().setVisible(false);
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.exit(0);
    }

    static void auth(String username, String password) throws AuthenticationException {
        if (password != null) {
            Authenticator authenticator = new Authenticator(Authenticator.MOJANG_AUTH_URL, AuthPoints.NORMAL_AUTH_POINTS);
            AuthResponse response = authenticator.authenticate(AuthAgent.MINECRAFT, username,password, "");
            authInfos = new AuthInfos(response.getSelectedProfile().getName(), response.getAccessToken(),response.getSelectedProfile().getId());
        } else {
            authInfos = new AuthInfos(username, "sry","nope");
        }
    }

    static CrashReporter getCrashReporter() {
        return crashReporter;
    }
}
