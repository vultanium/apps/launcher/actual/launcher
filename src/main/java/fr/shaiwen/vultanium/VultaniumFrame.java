package fr.shaiwen.vultanium;

import fr.theshark34.swinger.Swinger;
import fr.theshark34.swinger.animation.Animator;
import fr.theshark34.swinger.util.WindowMover;

import javax.swing.*;
import java.io.File;

public class VultaniumFrame extends JFrame {
    private static VultaniumFrame insstance;
    private VultaniumPanel vultaniumPanel;

    static File ramFile = new File(Vultanium.DIR, "ram.txt");

    private VultaniumFrame() {
        this.setTitle("Vultanium | Launcher");
        this.setSize(1280,720);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);
        this.setIconImage(Swinger.getResource("logo.png"));
        vultaniumPanel = new VultaniumPanel();
        this.setContentPane(vultaniumPanel);

        WindowMover mover = new WindowMover(this);
        this.addMouseListener(mover);
        this.addMouseMotionListener(mover);

        this.setVisible(true);

        Animator.fadeInFrame(this,Animator.NORMAL);
    }

    public static void main(String[] args) {
        /*
        VultaniumAPI vultaniumAPI = new VultaniumAPI();
        String maintenance = vultaniumAPI.read();

        if (maintenance.equalsIgnoreCase("true")) {
            JOptionPane.showMessageDialog(getInstance(), "Le serveur est en maintenance");
        }
        */
    	
        Swinger.setSystemLookNFeel();
        Swinger.setResourcePath("/fr/shaiwen/vultanium/ressources/");
        insstance = new VultaniumFrame();
    }

    static VultaniumFrame getInstance() {
        return insstance;
    }

    VultaniumPanel getVultaniumPanel() {
        return vultaniumPanel;
    }
}
